/*
  +0000000FF6          // this board, set onboard D13 LED on
  +0000000003          // this board, set onboard D13 LED off
  +00000A0FF6          // this board, all LEDs (including D13), all segments on
  +00000A0003          // this board, all LEDs (including D13), all segments off

                     //next board version of above commands
  +0000100FF7
  +0000100002
*/

#define MaxX 9
#define MaxY 9

#define Streamer 0

byte Streamers[18] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

byte ValueBufferA[81];
byte ValueBufferB[81];

byte BrightnessBufferA[81];
byte BrightnessBufferB[81];

// CMD:
// 0: display raw bits (DotAGDBCFE)
// 1: display a static digit (0..9), use GetRawDigit to translate to raw bits
// 2: increment a digit (0..9)
// 3: decrement a digit (0..9)
// 4: increment a digit (0..9) do not display 0
// 5: decrement a digit (0..9) do not display 0
// A: write to A arrays (LED and VALUE commands not important)
// B: write to B arrays (LED and VALUE commands not important)
// D: set delay (LEDTimerValues)
// F: flip A and B arrays (LED and VALUE commands not important)

// http://forum.arduino.cc/index.php?topic=6549.msg51570#msg51570
// defines for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif

void setupFastADC()
{
  // http://forum.arduino.cc/index.php?topic=6549.msg51570#msg51570
  // set prescale to 16
  sbi(ADCSRA, ADPS2) ;
  cbi(ADCSRA, ADPS1) ;
  cbi(ADCSRA, ADPS0) ;
}

// GetRawDigit with a switch is faster than
// const unsigned int Digits[] PROGMEM = {
//  B1011111, B0001100, B1111001, B1111100, B0101110, B1110110, B1110111, B1001100, B1111111, B1101110
// };
// return pgm_read_byte_near(Digits + d);

byte GetRawDigit(byte d)
{
  switch (d)
  {
    case 0:
      return B1011111;
    case 1:
      return B0001100;
    case 2:
      return B1111001;
    case 3:
      return B1111100;
    case 4:
      return B0101110;
    case 5:
      return B1110110;
    case 6:
      return B1110111;
    case 7:
      return B1001100;
    case 8:
      return B1111111;
    case 9:
      return B1101110;
    default:
      return 0;
  }
}

byte getOffset(byte x, byte y)
{
  return y * 9 + x;
}

void InitValueBufferA()
{
  for (byte i = 0; i < 81; i++)
  {
    ValueBufferA[i] = 0;
  }
}

void InitValueBufferB()
{
  for (byte i = 0; i < 81; i++)
  {
    ValueBufferB[i] = ValueBufferA[i] - 1;
  }
}

void InitBrightnessBufferA()
{
  for (byte i = 0; i < 81; i++)
  {
    BrightnessBufferA[i] = 0;
  }
}

void InitBrightnessBufferB()
{
  for (byte i = 0; i < 81; i++)
  {
    BrightnessBufferB[i] = BrightnessBufferA[i] - 1;
  }
}

void InitBuffers()
{
  InitValueBufferA();
  InitValueBufferB();


  InitBrightnessBufferA();
  InitBrightnessBufferB();
}

void SetValue(byte x, byte y, byte val)
{
  if ((x < MaxX) && (y < MaxY))
  {
    ValueBufferA[getOffset(x, y)] = val;
  }
}

void SetBrightness(byte x, byte y, byte val)
{
  if ((x < MaxX) && (y < MaxY))
  {
    BrightnessBufferA[getOffset(x, y)] = val;
  }
}

void UpdateScreen()
{
  SendCommand(0xFFFFF, 0, 0xB, 0);
  for (byte x = 0; x < 9; x++)
  {
    for (byte y = 0; y < 9; y++)
    {
      byte offset = getOffset(x, y);
      if (ValueBufferA[offset] != ValueBufferB[offset])
      {
        SendCommandXY(x, y, 0, ValueBufferA[offset]);
        ValueBufferB[offset] = ValueBufferA[offset];
      }
      if (BrightnessBufferA[offset] != BrightnessBufferB[offset])
      {
        SendCommandXY(x, y, 0xE, BrightnessBufferA[offset]);
        BrightnessBufferB[offset] = BrightnessBufferA[offset];
      }
    }
  }
  SendCommand(0xFFFFF, 0, 0xC, 0);
}

unsigned long time1;
unsigned long time2;

void setup() {
  // put your setup code here, to run once:

  unsigned long ADDR;
  byte LED;
  byte CMD;
  byte VAL;

  Serial.begin(115200);
  setupFastADC();

  InitBuffers();

  // Send a clear screen command continuously for 1 second to give all the boards a chance to boot up.
  BootCommand(0xFFFFF, 0xA, 0x0, 0x00);

  /*
    SendCommand(0xFFFFF, 0xA, 0xB, 0xff);
    SendCommand(0xFFFFF, 0xA, 0x0, 0x00);
    SendCommand(0xFFFFF, 0xA, 0xA, 0x00);
    SendCommand(0xFFFFF, 0xA, 0x0, 0xff);
    delay(1000);
  */

  UpdateScreen();

  /*
    // all boards, led is n/a, command A (select A array), val is n/a
    SendCommand(0xFFFFF, 0, 0xA, 0);

    // all boards, all led, command 0 (set raw), all segments on
    SendCommand(0xFFFFF, 0xA, 0x0, 0xFF);

    // all boards, led is n/a, command B (select B array), val is n/a
    SendCommand(0xFFFFF, 0, 0xB, 0);

    for (byte j = 0; j < 9; j++)
    {
    for (byte i = 1; i < 10; i++)
    {
      ADDR = j;
      LED = i;
      CMD = 1;
      VAL = i;
      SendCommand(ADDR, LED, CMD, VAL);
    }
    }

    // all boards, all led, command 0 (set raw), all segments on
    SendCommand(0x00000, 4, 0, 0);
    SendCommand(0x00000, 5, 0, 55);
    SendCommand(0x00000, 6, 0, 115);

    SendCommand(0x00001, 4, 0, 79);
    SendCommand(0x00001, 5, 0, 76);
    SendCommand(0x00001, 6, 0, 12);

    SendCommand(0x00002, 4, 0, 115);
    SendCommand(0x00002, 5, 0, 62);
    SendCommand(0x00002, 6, 0, 0);

    // all boards, led is n/a, command A (select A array), val is n/a
    SendCommand(0xFFFFF, 0, 0xA, 0);


    for (byte x = 0; x < 9; x++)
    {
    for (byte y = 0; y < 9; y++)
    {
      SendCommandXY(x, y, 1, x);
    }
    }

  */

  time1 = millis();
  time2 = time1;
}

byte xo = 0;
byte yo = 0;

byte x = 0;
byte y = 0;

void Matrix1()
{

  if ((millis() - time2) > 450)
  {
    //SendCommand(0xFFFFF, 0, 0xF, 0);

    SetValue(xo, yo, 0);

    if (y > Streamer)
    {
      //SetValue(xo, yo - Streamer, 0);
    }
    xo = x; yo = y;

    SetBrightness(x, y, 40);

    y++;
    if (y == 6)
    {
      y = 0;
      x++;
      if (x == 3)
      {
        x = 0;
        y = 0;
      }
    }

    SetValue(x, y, 117);
    SetBrightness(x, y, 254);

    time2 += 450;
  }

  if ((millis() - time1) > 150)
  {
    SetValue(x, y, GetRawDigit(random(9)));
    time1 += 150;
  }

}

void loop() {
  // put your main code here, to run repeatedly:

  Matrix1();
  UpdateScreen();
}
