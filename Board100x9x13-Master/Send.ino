// CMD:
// 0: display raw bits (DotAGDBCFE)
// 1: display a static digit (0..9), use GetRawDigit to translate to raw bits
// 2: increment a digit (0..9)
// 3: decrement a digit (0..9)
// 4: increment a digit (0..9) do not display 0
// 5: decrement a digit (0..9) do not display 0
// A: write to A arrays (LED and VALUE commands not important)
// B: write to B arrays (LED and VALUE commands not important)
// D: set delay (LEDTimerValues)
// F: flip A and B arrays (LED and VALUE commands not important)

//                      +   [ADDRESS]               [LED][CMD] [VAL]     [CHKSUM]
//                           6    5    5    3    5    9    A    F    F    F
char TxOneFrame[12] = {'+', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0};

byte TxChecksum()
{
  byte v = 0;
  for (byte i = 1; i < 10; i++)
  {
    v = v + TxOneFrame[i];
    v = v ^ (v >> 4);
    v &= 15;
  }

  return v;
}

void TxSetChecksum()
{
  byte v = TxChecksum();

  if (v > 9)
  {
    v += '7';
  }
  else
  {
    v += '0';
  }

  TxOneFrame[10] = v;
}

void TxDecToHex(unsigned long VAL, byte startp, byte endp)
{
  byte v;

  //Serial.println(F("D2H"));

  //Serial.println(VAL);
  //Serial.println(startp);
  //Serial.println(endp);

  do
  {
    v = VAL & 15;

    VAL = VAL >> 4;

    //Serial.println(v);

    if (v > 9)
    {
      v += 'A' - 10;
    }
    else
    {
      v += '0';
    }

    //Serial.println(endp);
    //Serial.println((char)v);

    TxOneFrame[endp] = v;

    if ((endp == 1) || (endp == startp))
    {
      break;
    }
    else
    {
      endp--;
    }

  } while (1);
}

void SetADDR(unsigned long ADDR)
{
  TxDecToHex(ADDR, 1, 5);
}

void SetLED(byte LED)
{
  TxDecToHex(LED, 6, 6);
}

void SetCMD(byte CMD)
{
  TxDecToHex(CMD, 7, 7);
}

void SetVal(byte VAL)
{
  TxDecToHex(VAL, 8, 9);
}

void MakeTxFrame(unsigned long ADDR, byte LED, byte CMD, byte VAL)
{
  //Serial.println(ADDR);
  //Serial.println(LED);
  //Serial.println(CMD);
  //Serial.println(VAL);

  TxDecToHex(ADDR, 1, 5);
  TxDecToHex(LED, 6, 6);
  TxDecToHex(CMD, 7, 7);
  TxDecToHex(VAL, 8, 9);

  TxSetChecksum();
}

void PrintTxFrame()
{
  static unsigned long lastTx = 0;

  do
  {
  } while ((micros() - lastTx) < 4000);

  //Serial.println(micros() - lastTx);

  Serial.println(TxOneFrame);

  lastTx = micros();
}

void Translate(byte x, byte y)
{
  byte xcol, xboard;
  byte yrow, yboard;

  unsigned int ADDR;
  byte LED;

  //unsigned long time1, time2;

  //time1 = micros();

  xcol = x % 3;
  xboard = x / 3;

  yrow = y % 3;
  yboard = y / 3;

  LED = (xcol + 1) + (yrow * 3);

  // code below assumes board order:
  // 0  1  2   xboard+ ->
  // 3  4  5
  // 6  7  8   |
  // yboard+   V

  if ((xboard == 0) && (yboard == 0))
  {
    ADDR = 0;
  }

  if ((xboard == 1) && (yboard == 0))
  {
    ADDR = 10; // BUG: restore this to 1 for 3x3 array, set to 10 for testing with 2 boards
  }

  if ((xboard == 2) && (yboard == 0))
  {
    ADDR = 2;
  }

  if ((xboard == 0) && (yboard == 1))
  {
    ADDR = 1; // BUG: restore this to 3 for 3x3 array set to 1 for testing with 2 boards
  }

  if ((xboard == 1) && (yboard == 1))
  {
    ADDR = 4;
  }

  if ((xboard == 2) && (yboard == 1))
  {
    ADDR = 5;
  }

  if ((xboard == 0) && (yboard == 2))
  {
    ADDR = 6;
  }

  if ((xboard == 1) && (yboard == 2))
  {
    ADDR = 7;
  }

  if ((xboard == 2) && (yboard == 2))
  {
    ADDR = 8;
  }

  //time2 = micros();

  //Serial.println(xcol);
  //Serial.println(xboard);
  //Serial.println(yrow);
  //Serial.println(yboard);

  //Serial.println(ADDR);
  //Serial.println(LED);

  //Serial.println(time2 - time1);


  SetADDR(ADDR);
  SetLED(LED);
}

void SendCommand(unsigned long ADDR, byte LED, byte CMD, byte VAL)
{
  MakeTxFrame(ADDR, LED, CMD, VAL);
  PrintTxFrame();
}

void SendCommandXY(byte XL, byte YL, byte CMD, byte VAL)
{
  Translate(XL, YL);
  SetCMD(CMD);
  SetVal(VAL);
  TxSetChecksum();
  PrintTxFrame();
}

void BootCommand(unsigned long ADDR, byte LED, byte CMD, byte VAL)
{
  unsigned long time1 = 0;

  time1 = micros();

  do
  {
    SendCommand(0xFFFFF, 0, 0xA, 00); //Send a comnmand to select A buffer, ensure next command is visible
    SendCommand(ADDR, LED, CMD, VAL);
  } while ((micros() - time1) < 1000000);
}
