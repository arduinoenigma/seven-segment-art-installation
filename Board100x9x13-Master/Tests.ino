void BitOperations()
{
  Serial.println(1 | 2);          // 3
  Serial.println(1 << 6);         // 64
  Serial.println((1 | 2) << 6);   // 192
  Serial.println(1 ^ 3);          // 2
  Serial.println(255 & 3);        // 3
  Serial.println(254 & 3);        // 2
}

void ModDiv()
{
  for (byte i = 0; i < 9; i++)
  {
    Serial.println(i / 3);
  }


  for (byte i = 0; i < 9; i++)
  {
    Serial.println(i % 3);
  }

}

void showADC()
{
  Serial.print(analogRead(0));
  Serial.print(" ");
  Serial.print(analogRead(1));
  Serial.print(" ");
  Serial.print(analogRead(2));
  Serial.print(" ");
  Serial.print(analogRead(3));
  Serial.print(" ");
  Serial.print(analogRead(4));
  Serial.print(" ");
  Serial.print(analogRead(5));
  Serial.print(" ");
  Serial.print(analogRead(6));
  Serial.print(" ");
  Serial.print(analogRead(7));
  Serial.println(" ");
}

byte showrandom()
{
  static byte prev = 0;
  byte now;

  Serial.println("analog read:");
  for (byte i = 0; i < 250; i++)
  {
    //prev = (prev) ^ ((analogRead(0) & 3) << 6) | ((analogRead(1) & 3) << 5) | ((analogRead(2) & 3) << 4) | ((analogRead(3) & 3) << 3) | ((analogRead(5) & 3) << 1) | ((analogRead(6) & 3));
    prev = ((analogRead(0) & 3) << 6) | ((analogRead(1) & 3) << 5) | ((analogRead(2) & 3) << 4) | ((analogRead(3) & 3) << 3) | ((analogRead(5) & 3) << 1) | ((analogRead(6) & 3));
    Serial.println(prev);
  }

  Serial.println("fixed seed");
  randomSeed(7);
  for (byte i = 0; i < 250; i++)
  {
    Serial.println(random(255));
  }

  Serial.println("random seed");
  randomSeed(analogRead(0));
  for (byte i = 0; i < 250; i++)
  {
    Serial.println(random(255));
  }
}

void debugTxFrame()
{
  Serial.print(F("fr:"));
  Serial.println(TxOneFrame);
}

void TestTxCommands()
{
  unsigned long ADDR;
  byte LED;
  byte CMD;
  byte VAL;

  Serial.println(F("same number in all positions"));

  ADDR = 0;
  LED = 0xA;
  CMD = 1;

  for (byte i = 1; i < 10; i++)
  {
    VAL = i;
    MakeTxFrame(ADDR, LED, CMD, VAL);
    PrintTxFrame();
  }

  Serial.println(F("single number in all positions"));

  for (byte i = 1; i < 10; i++)
  {
    Serial.print(F("LED:"));
    Serial.println(i);

    for (byte j = 0; j < 10; j++)
    {
      LED = i;
      VAL = j;
      MakeTxFrame(ADDR, LED, CMD, VAL);
      PrintTxFrame();
    }
  }
}
