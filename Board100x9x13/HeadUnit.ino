//                      +   [ADDRESS]               [LED][CMD] [VAL]     [CHKSUM]
//                           6    5    5    3    5    9    A    F    F    F
char TxOneFrame[12] = {'+', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0};

byte TxChecksum()
{
  byte v = 0;
  for (byte i = 1; i < 10; i++)
  {
    v = v + TxOneFrame[i];
    v = v ^ (v >> 4);
    v &= 15;
  }

  return v;
}

void TxSetChecksum()
{
  byte v = TxChecksum();

  if (v > 9)
  {
    v += '7';
  }
  else
  {
    v += '0';
  }

  TxOneFrame[10] = v;
}

void TxDecToHex(unsigned long VAL, byte startp, byte endp)
{
  byte v;

  do
  {
    v = VAL & 15;

    VAL = VAL >> 4;

    if (v > 9)
    {
      v += 'A' - 10;
    }
    else
    {
      v += '0';
    }

    TxOneFrame[endp] = v;

    if ((endp == 1) || (endp == startp))
    {
      break;
    }
    else
    {
      endp--;
    }

  } while (1);
}

void MakeTxFrame(unsigned long ADDR, byte LED, byte CMD, byte VAL)
{
  TxDecToHex(ADDR, 1, 5);
  TxDecToHex(LED, 6, 6);
  TxDecToHex(CMD, 7, 7);
  TxDecToHex(VAL, 8, 9);

  TxSetChecksum();
}

void PrintTxFrame()
{
  Serial.println(TxOneFrame);

  for (byte i = 0; i < 11; i++)
  {
    //Serial.print((byte)TxOneFrame[i]);

    //UpdateFade();
    //UpdateOneLED();
    //AnimateLED(); // call it every loop, exits with no action if timer has not reached correct value
    CmdStateMachine(TxOneFrame[i]);
  }

  for (int i = 0; i < 5000; i++)
  {
    UpdateFade();
    UpdateOneLED();
    AnimateLED(); // call it every loop, exits with no action if timer has not reached correct value
  }
}

// CMD:
// 0: display raw bits (DotAGDBCFE)
// 1: display a static digit (0..9), use GetRawDigit to translate to raw bits
// 2: increment a digit (0..9)
// 3: decrement a digit (0..9)
// 4: increment a digit (0..9) do not display 0
// 5: decrement a digit (0..9) do not display 0
// A: write to A arrays (LED and VALUE commands not important)
// B: write to B arrays (LED and VALUE commands not important)
// D: set delay (LEDTimerValues)
// F: flip A and B arrays (LED and VALUE commands not important)

void TestTxCommands()
{
  unsigned long ADDR;
  byte LED;
  byte CMD;
  byte VAL;

  Serial.println(F("same number in all positions"));

  ADDR = 0;
  LED = 0xA;
  CMD = 1;

  for (byte i = 1; i < 10; i++)
  {
    VAL = i;
    MakeTxFrame(ADDR, LED, CMD, VAL);
    PrintTxFrame();
  }

  Serial.println(F("single number in all positions"));

  for (byte i = 1; i < 10; i++)
  {
    Serial.print(F("LED:"));
    Serial.println(i);

    for (byte j = 0; j < 10; j++)
    {
      LED = i;
      VAL = j;
      MakeTxFrame(ADDR, LED, CMD, VAL);
      PrintTxFrame();
    }
  }

}

