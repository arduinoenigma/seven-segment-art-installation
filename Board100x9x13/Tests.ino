// test functions, everything in this file can be deleted in a production version

const unsigned int Digits2[] PROGMEM = {
  B1011111, B0001100, B1111001, B1111100, B0101110, B1110110, B1110111, B1001100, B1111111, B1101110
};

byte getDigit1(byte d)
{
  switch (d)
  {
    case 0:
      return B1011111;
    case 1:
      return B0001100;
    case 2:
      return B1111001;
    case 3:
      return B1111100;
    case 4:
      return B0101110;
    case 5:
      return B1110110;
    case 6:
      return B1110111;
    case 7:
      return B1001100;
    case 8:
      return B1111111;
    case 9:
      return B1101110;
    default:
      return 0;
  }
}

byte getDigit2(byte d)
{
  return pgm_read_byte_near(Digits2 + d);
}

void testGetDigits()
{
  unsigned long t1, t2;
  unsigned int i;
  unsigned int k;

  k = 0;
  t1 = micros();
  for (i = 0; i < 60000; i++)
  {
    k += getDigit1(i & 3);
  }
  t2 = micros();
  Serial.println(k);
  Serial.println(t2 - t1);

  k = 0;
  t1 = micros();
  for (i = 0; i < 60000; i++)
  {
    k += getDigit2(i & 3);
  }
  t2 = micros();
  Serial.println(k);
  Serial.println(t2 - t1);

}

byte incdec1state = 0;
byte incdec1(byte v)
{
  if (incdec1state == 1)
  {
    incdec1state = 0;
    return v + 1;
  }
  else
  {
    incdec1state = 1;
    return v - 1;
  }
}

//slower, but not by much
byte incdec2(byte v)
{
  static byte incdec2state = 0;
  if (incdec2state == 1)
  {
    incdec2state = 0;
    return v + 1;
  }
  else
  {
    incdec2state = 1;
    return v - 1;
  }
}

void testIncDec()
{
  unsigned long t1, t2;
  unsigned int i;
  unsigned int k;

  k = 0;
  t1 = micros();
  for (i = 0; i < 60000; i++)
  {
    k += incdec1(i & 3);
  }
  t2 = micros();
  Serial.println(k);
  Serial.println(t2 - t1);

  k = 0;
  t1 = micros();
  for (i = 0; i < 60000; i++)
  {
    k += incdec2(i & 3);
  }
  t2 = micros();
  Serial.println(k);
  Serial.println(t2 - t1);

}

void testEEPROMTimes()
{
  unsigned long t1, t2;
  byte v = 0;

  t1 = micros();
  v = EEPROM.read(1948);
  t2 = micros();
  Serial.println(t2 - t1);

  v++;

  t1 = micros();
  EEPROM.write(1948, v);
  t2 = micros();
  Serial.println(t2 - t1);

}

bool IsDigit1(char c)
{
  return ((c > 47) && (c < 58)) ? true : false;
}

void debugTxFrame()
{
  Serial.print(F("fr:"));
  Serial.println(TxOneFrame);
}

void dumpOneFrame()
{
  for (byte i = 0; i < 12; i++)
  {
    Serial.print((byte)OneFrame[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));
}

void dumpLEDValues()
{
  for (byte i = 0; i < 10; i++)
  {
    Serial.print((byte)LEDValuesA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));
}

void dumpAllArrays()
{
  Serial.print("FadeTimer: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(FadeTimer[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDValuesA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDValuesA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDValuesB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDValuesB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDCommandsA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDCommandsA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDCommandsB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDCommandsB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimersA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimersA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimersB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimersB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimerValuesA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimerValuesA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimerValuesB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimerValuesB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("FixedFadeA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(FixedFadeA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("FixedFadeB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(FixedFadeB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));
}

//test function to set value of OneFrame and correct checksum
void Checksum(const __FlashStringHelper *ifsh)
{
  PGM_P p = reinterpret_cast<PGM_P>(ifsh);

  byte cnt = 1;
  byte v = 0;

  //dumpOneFrame();

  while (cnt < 11) {
    byte c = pgm_read_byte(p++);
    if (c == 0) break;
    OneFrame[cnt++] = c;
  }

  v = Checksum();

  Serial.println(OneFrame);
  dumpOneFrame();
  Serial.println(v);
  SetChecksum();
  Serial.println(OneFrame);
  dumpOneFrame();
  Serial.println(F(""));
}

void ChecksumTests()
{
  Serial.println(F("Checksum Tests"));

  Checksum(F("1111111111"));
  Checksum(F("1111111112"));
  Checksum(F("1111111113"));
  Checksum(F("1111111121"));
  Checksum(F("1111111211"));
  Checksum(F("1111112111"));
  Checksum(F("1111121111"));
  Checksum(F("1111211111"));
  Checksum(F("1112111111"));
  Checksum(F("1121111111"));
  Checksum(F("1211111111"));
  Checksum(F("2111111111"));
  Checksum(F("3111111111"));
  Checksum(F("1311111111"));
  Checksum(F("1131111111"));
  Checksum(F("1113111111"));
  Checksum(F("1111311111"));
  Checksum(F("1111131111"));
  Checksum(F("1111113111"));
  Checksum(F("1211112111"));
  Checksum(F("0000000000"));

  Serial.println(F("Test Commands"));

  Checksum(F("0000000FF0"));
  Checksum(F("0000000000"));
  Checksum(F("000000AFF0"));

  Checksum(F("0000100FF0"));
  Checksum(F("0000100000"));
  Checksum(F("00001A0FF0"));

  Checksum(F("1211FFFFF1"));
  Checksum(F("9999900FFA"));
  Checksum(F("FFFFF00FFA"));
  Checksum(F("PRG0800FFA"));

  Serial.println(F("HexToDec Tests"));

  Checksum(F("1211112001"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211112011"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211112101"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211112091"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211112901"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("12111120A1"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211112A01"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("12111120F1"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211112F01"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211112FF1"));
  Serial.println(HexToDec(8, 9));
  Checksum(F("1211118001"));
  Serial.println(HexToDec(7, 9));

  Checksum(F("PRG0800FFA"));
  Serial.println(HexToDec(5, 7));
  Serial.println(HexToDec(8, 9));
}
