A Seven Segment Art Installation

A 100x100mm board is the basis for this project, a large installation of animated seven segment displays.

Each board has an Arduino Nano and 9 Seven-Segment Displays. The Boards are daisy chained through three-pin headers on the sides. The pins carry power, ground and a serial port signal.
The software for the tile boards is here:
[https://gitlab.com/arduinoenigma/seven-segment-art-installation/tree/master/Board100x9x13](https://gitlab.com/arduinoenigma/seven-segment-art-installation/tree/master/Board100x9x13)

The first board on the chain has two Arduino Nanos. The first one sends the instructions on which digits to light up,the second one drives the displays and relays the instructions to the next board down the chain.
The master Arduino runs this software:
[https://gitlab.com/arduinoenigma/seven-segment-art-installation/tree/master/Board100x9x13-Master](https://gitlab.com/arduinoenigma/seven-segment-art-installation/tree/master/Board100x9x13-Master)

Follow this project here:

[https://hackaday.io/project/163549-a-seven-segment-art-installation](https://hackaday.io/project/163549-a-seven-segment-art-installation)
