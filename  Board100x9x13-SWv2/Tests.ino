void dumpAllArrays()
{
  Serial.print("FadeTimer: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(FadeTimer[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDValuesA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDValuesA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDValuesB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDValuesB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDCommandsA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDCommandsA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDCommandsB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDCommandsB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimersA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimersA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimersB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimersB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimerValuesA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimerValuesA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("LEDTimerValuesB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(LEDTimerValuesB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("FixedFadeA: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(FixedFadeA[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));

  Serial.print("FixedFadeB: ");
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(FixedFadeB[i]);
    Serial.print(F(" "));
  }
  Serial.println(F(""));
}


