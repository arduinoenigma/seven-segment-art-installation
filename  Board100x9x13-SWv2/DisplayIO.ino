//
// A0 = D14
// A1 = D15
// A2 = D16
// A3 = D17
// A4 = D18
// A5 = D19
// A6 = D20
// A7 = D21
//

#if defined(PRODUCTIONBOARD)

//9X9 RED BOARD
GPIO<BOARD::D6>  SegmentA;
GPIO<BOARD::D8>  SegmentB;
GPIO<BOARD::D7>  SegmentC;
GPIO<BOARD::D4>  SegmentD;
GPIO<BOARD::D2>  SegmentE;
GPIO<BOARD::D5>  SegmentF;
GPIO<BOARD::D3>  SegmentG;
GPIO<BOARD::D9>  SegmentDP;

GPIO<BOARD::D16> Digit1;     // A2
GPIO<BOARD::D18> Digit2;     // A4
GPIO<BOARD::D19> Digit3;     // A5
GPIO<BOARD::D15> Digit4;     // A1
GPIO<BOARD::D17> Digit5;     // A3
GPIO<BOARD::D10> Digit6;     // D10
GPIO<BOARD::D14> Digit7;     // A0
GPIO<BOARD::D12> Digit8;     // D12
GPIO<BOARD::D11> Digit9;     // D11

#define SegmentOn high
#define SegmentOff low
#define DigitOn low
#define DigitOff high

#else

//SINCLAIR CALCULATOR V5 HARDWARE
GPIO<BOARD::D5>  SegmentA;
GPIO<BOARD::D8>  SegmentB;
GPIO<BOARD::D10> SegmentC;
GPIO<BOARD::D19> SegmentD;
GPIO<BOARD::D9>  SegmentE;
GPIO<BOARD::D6>  SegmentF;
GPIO<BOARD::D14> SegmentG;
GPIO<BOARD::D18> SegmentDP;

GPIO<BOARD::D2>  Digit1;
GPIO<BOARD::D3>  Digit2;
GPIO<BOARD::D4>  Digit3;
GPIO<BOARD::D17> Digit4;
GPIO<BOARD::D16> Digit5;
GPIO<BOARD::D7>  Digit6;
GPIO<BOARD::D15> Digit7;
GPIO<BOARD::D11> Digit8;
GPIO<BOARD::D12> Digit9;

#define SegmentOn low
#define SegmentOff high
#define DigitOn high
#define DigitOff low

#endif

GPIO<BOARD::D13> BuiltIn;

void heartBeatOutput()
{
  BuiltIn.output();
}

void heartBeatOn() {
  BuiltIn.DigitOn();
}

void heartBeatOff() {
  BuiltIn.DigitOff();
}

void allSegmentOutput() {
  SegmentA.output();
  SegmentB.output();
  SegmentC.output();
  SegmentD.output();
  SegmentE.output();
  SegmentF.output();
  SegmentG.output();
  SegmentDP.output();
}

void allSegmentInput() {
  SegmentA.input();
  SegmentB.input();
  SegmentC.input();
  SegmentD.input();
  SegmentE.input();
  SegmentF.input();
  SegmentG.input();
  SegmentDP.input();
}

void allDigitOutput() {
  Digit1.output();
  Digit2.output();
  Digit3.output();
  Digit4.output();
  Digit5.output();
  Digit6.output();
  Digit7.output();
  Digit8.output();
  Digit9.output();
  //BuiltIn.output();
}

void allDigitInput() {
  Digit1.input();
  Digit2.input();
  Digit3.input();
  Digit4.input();
  Digit5.input();
  Digit6.input();
  Digit7.input();
  Digit8.input();
  Digit9.input();
  //BuiltIn.input();
}

void allSegmentOff() {
  SegmentA.SegmentOff();
  SegmentB.SegmentOff();
  SegmentC.SegmentOff();
  SegmentD.SegmentOff();
  SegmentE.SegmentOff();
  SegmentF.SegmentOff();
  SegmentG.SegmentOff();
  SegmentDP.SegmentOff();
}

void allDigitOff() {
  Digit1.DigitOff();
  Digit2.DigitOff();
  Digit3.DigitOff();
  Digit4.DigitOff();
  Digit5.DigitOff();
  Digit6.DigitOff();
  Digit7.DigitOff();
  Digit8.DigitOff();
  Digit9.DigitOff();
}

void selectDigit(byte digit) {

  allDigitOff();
  allDigitInput();

  switch (digit) {
    case 1:
      Digit1.DigitOn();
      Digit1.output();
      break;
    case 2:
      Digit2.DigitOn();
      Digit2.output();
      break;
    case 3:
      Digit3.DigitOn();
      Digit3.output();
      break;
    case 4:
      Digit4.DigitOn();
      Digit4.output();
      break;
    case 5:
      Digit5.DigitOn();
      Digit5.output();
      break;
    case 6:
      Digit6.DigitOn();
      Digit6.output();
      break;
    case 7:
      Digit7.DigitOn();
      Digit7.output();
      break;
    case 8:
      Digit8.DigitOn();
      Digit8.output();
      break;
    case 9:
      Digit9.DigitOn();
      Digit9.output();
      break;
  }
}

void displayRaw(byte raw)
{
  if (raw & B10000000)
  {
    SegmentDP.SegmentOn();
  }
  else
  {
    SegmentDP.SegmentOff();
  }

  if (raw & B01000000)
  {
    SegmentA.SegmentOn();
  }
  else
  {
    SegmentA.SegmentOff();
  }

  if (raw & B00100000)
  {
    SegmentG.SegmentOn();
  }
  else
  {
    SegmentG.SegmentOff();
  }

  if (raw & B00010000)
  {
    SegmentD.SegmentOn();
  }
  else
  {
    SegmentD.SegmentOff();
  }

  if (raw & B00001000)
  {
    SegmentB.SegmentOn();
  }
  else
  {
    SegmentB.SegmentOff();
  }

  if (raw & B00000100)
  {
    SegmentC.SegmentOn();
  }
  else
  {
    SegmentC.SegmentOff();
  }

  if (raw & B00000010)
  {
    SegmentF.SegmentOn();
  }
  else
  {
    SegmentF.SegmentOff();
  }

  if (raw & B00000001)
  {
    SegmentE.SegmentOn();
  }
  else
  {
    SegmentE.SegmentOff();
  }
}

