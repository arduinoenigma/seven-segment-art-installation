#include "GPIO.h"
#include <EEPROM.h>

//COMMENT LINE BELOW TO RUN ON SINCLAIR
#define PRODUCTIONBOARD

#define AnimateLEDTimerValue 1280
#define FadeTimerValue 50

#define TASKLOADEEPROM 0
#define TASKSAVEEEPROM 1
#define EEPROMWRITE 2
#define TASKCMDSM 3
#define TASKSERIALPRINT 4

unsigned long exectimer[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned long maxtimes[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

//                    +   [ADDRESS]               [LED][CMD] [VAL]     [CHKSUM]
//                         6    5    5    3    5    9    A    F    F    F
char OneFrame[12] = {'+', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0};

// LED:
// 0: built in D13 LED
// 1..9 external display
// A: all LED

// CMD:
// 0: display raw bits (DotAGDBCFE)
// 1: display a static digit (0..9), use GetRawDigit to translate to raw bits
// 2: increment a digit (0..9)
// 3: decrement a digit (0..9)
// 4: increment a digit (0..9) do not display 0
// 5: decrement a digit (0..9) do not display 0
// A: write to A arrays (LED and VALUE commands not important)
// B: write to B arrays (LED and VALUE commands not important)
// C: copy array (B to A if VALUE is 0, A to B otherwise)
// D: set delay (LEDTimerValues)
// E: set fixed brigthness (FixedFade)
// F: flip A and B arrays (LED and VALUE commands not important)

// +0000000FF6          // this board, set onboard D13 LED on
// +0000000003          // this board, set onboard D13 LED off
// +00000A0FF6          // this board, all LEDs (including D13), all segments on
// +00000A0003          // this board, all LEDs (including D13), all segments on

// +000000A005          // this board, future commands modify A arrays
// +000000B006          // this board, future commands modify B arrays
// +000000F002          // this board, flip A arrays and B arrays
//
//                      //next board version of above commands
// +0000100FF7
// +0000100002
// +00001A0FF1
// +00001A0004
//                      //multicast version of above commands
// +FFFFF00FFE
// +FFFFF0000B
// +FFFFFA0FFE
// +FFFFFA000B
// +FFFFF0A00D
// +FFFFF0B00E
// +FFFFF0F00A

// +PRG0800FFC
// +PRG0FFF00D          // saves array values to eeprom, reloads them on startup

// A Arrays (updated live)
// B Arrays (not displayed, can be flipped into A Arrays

// Brightness level, 254 is maximum value, automatically incremented, set to 1 to slowly fade to brightness, see FixedFade to disregard this
byte FadeTimer[10] = {254, 254, 254, 254, 254, 254, 254, 254, 254, 254};

// values to show on the LEDs, position 0 is built in LED at D13
byte LEDValuesA[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
byte LEDValuesB[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// commands to perform on the LEDs, position 0 is built in LED at D13
byte LEDCommandsA[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
byte LEDCommandsB[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

// timers for commands to perform on the LEDs, position 0 is built in LED at D13
byte LEDTimersA[10] = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
byte LEDTimersB[10] = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10};

// timer values for commands to perform on the LEDs, position 0 is built in LED at D13
byte LEDTimerValuesA[10] = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
byte LEDTimerValuesB[10] = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10};

// Set this to a non-zero value to disregard FadeTimer
byte FixedFadeA[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
byte FixedFadeB[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


// TODO:
// Headunit: encodecommand(address,command,value) // converts number to string, fills buffer and calculates checksum, address 100000 is turned to FFFFF multicast
// commands (+ indicates done):
// +SetValueA (displayed right away) (SET CMD A)
// +SetValueB (never displayed) (SET CMD B)
// +SetCommandA
// +SetCommandB
// +SetTimeA (SET CMD D)
// +SetTimeB
// +FlipAB    (swaps values A & B) (allows slowly setting B values and instantaneously display all, particularly if multicast)
// +IncrementA (sets same command, increment, on commandA array
// +IncrementB
// ScrollUpAB
// ScrollDnAB
// +add ValueA, ValueB, CommandA, CommandB. TimeA, TimeB array
// +add global variable timer, everytime it reaches 0, decrement all entries in time array (allows more range on 8bit value on time array)

enum CmdSM {
  CMDSMIDLE,
  CMDSMREADADDR,
  CMDSMREADREST,
  CMDSMPROCESS,
};

void entrytimer(byte task)
{
  exectimer[task] = micros();
}

void exittimer(byte task)
{
  unsigned long entry = exectimer[task];
  unsigned long now = micros();

  unsigned long t = now - entry;

  if (t > maxtimes[task])
  {
    maxtimes[task] = t;
  }
}

void printtimer()
{
  for (byte i = 0; i < 10; i++)
  {
    Serial.print(i);
    Serial.print(F(":"));
    Serial.println(maxtimes[i]);
  }
}

// GetRawDigit with a switch is faster than
// const unsigned int Digits[] PROGMEM = {
//  B1011111, B0001100, B1111001, B1111100, B0101110, B1110110, B1110111, B1001100, B1111111, B1101110
// };
// return pgm_read_byte_near(Digits + d);

byte GetRawDigit(byte d)
{
  switch (d)
  {
    case 0:
      return B1011111;
    case 1:
      return B0001100;
    case 2:
      return B1111001;
    case 3:
      return B1111100;
    case 4:
      return B0101110;
    case 5:
      return B1110110;
    case 6:
      return B1110111;
    case 7:
      return B1001100;
    case 8:
      return B1111111;
    case 9:
      return B1101110;
    default:
      return 0;
  }
}

unsigned int HexToDec(byte startp, byte endp)
{
  byte tv = 0;
  unsigned int v = 0;
  byte pos = startp;

  do
  {
    if ((pos > 10) || (pos > endp)) break;

    tv = OneFrame[pos++];
    tv = tv - '0';
    if (tv > 9) tv = tv - 7;

    v = v << 4;
    v = v | tv;

  } while (1);

  return v;
}

byte Checksum()
{
  byte v = 0;
  for (byte i = 1; i < 10; i++)
  {
    v = v + OneFrame[i];
    v = v ^ (v >> 4);
    v &= 15;
  }

  return v;
}

void SetChecksum()
{
  byte v = Checksum();

  if (v > 9)
  {
    v += '7';
  }
  else
  {
    v += '0';
  }

  OneFrame[10] = v;
}

void LoadFromEEPROM()
{
  int SaveAddr = 1930;
  byte val;

  entrytimer(TASKLOADEEPROM);

  for (byte SaveCounter = 0; SaveCounter < 110; SaveCounter++)
  {
    val = EEPROM.read(SaveAddr);
    SaveAddr++;

    if (SaveCounter < 10)
    {
      FadeTimer[SaveCounter] = val;
    }

    if ((SaveCounter > 9) && (SaveCounter < 20))
    {
      LEDValuesA[SaveCounter - 10] = val;
    }

    if ((SaveCounter > 19) && (SaveCounter < 30))
    {
      LEDCommandsA[SaveCounter - 20] = val;
    }

    if ((SaveCounter > 29) && (SaveCounter < 40))
    {
      LEDTimersA[SaveCounter - 30] = val;
    }

    if ((SaveCounter > 39) && (SaveCounter < 50))
    {
      LEDTimerValuesA[SaveCounter - 40] = val;
    }

    if ((SaveCounter > 49) && (SaveCounter < 60))
    {
      LEDValuesB[SaveCounter - 50] = val;
    }

    if ((SaveCounter > 59) && (SaveCounter < 70))
    {
      LEDCommandsB[SaveCounter - 60] = val;
    }

    if ((SaveCounter > 69) && (SaveCounter < 80))
    {
      LEDTimersB[SaveCounter - 70] = val;
    }

    if ((SaveCounter > 79) && (SaveCounter < 90))
    {
      LEDTimerValuesB[SaveCounter - 80] = val;
    }

    if ((SaveCounter > 89) && (SaveCounter < 100))
    {
      FixedFadeA[SaveCounter - 90] = val;
    }

    if ((SaveCounter > 99) && (SaveCounter < 110))
    {
      FixedFadeB[SaveCounter - 100] = val;
    }
  }

  exittimer(TASKLOADEEPROM);
}

//Save one byte of each array at a time...
//
//byte FadeTimer[10]
//byte LEDValuesA[10]
//byte LEDCommandsA[10]
//byte LEDTimersA[10]
//byte LEDTimerValuesA[10]
//
//byte LEDValuesB[10]
//byte LEDCommandsB[10]
//byte LEDTimersB[10]
//byte LEDTimerValuesB[10]

void SaveToEEPROM(byte go = 0)
{
  static byte SaveInProgress = 0;
  static byte SaveCounter = 0;
  static int SaveAddr = 1930;

  byte val = 0;

  entrytimer(TASKSAVEEEPROM);

  if (SaveInProgress == 0)
  {
    if (go == 1)
    {
      SaveInProgress = 1;
      SaveCounter = 0;
      SaveAddr = 1930;
      Serial.println(F("SVS"));
    }
  }
  else
  {
    entrytimer(5);

    if (SaveCounter < 10)
    {
      val = FadeTimer[SaveCounter];
    }

    if ((SaveCounter > 9) && (SaveCounter < 20))
    {
      val = LEDValuesA[SaveCounter - 10];
    }

    if ((SaveCounter > 19) && (SaveCounter < 30))
    {
      val = LEDCommandsA[SaveCounter - 20];
    }

    if ((SaveCounter > 29) && (SaveCounter < 40))
    {
      val = LEDTimersA[SaveCounter - 30];
    }

    if ((SaveCounter > 39) && (SaveCounter < 50))
    {
      val = LEDTimerValuesA[SaveCounter - 40];
    }

    if ((SaveCounter > 49) && (SaveCounter < 60))
    {
      val = LEDValuesB[SaveCounter - 50];
    }

    if ((SaveCounter > 59) && (SaveCounter < 70))
    {
      val = LEDCommandsB[SaveCounter - 60];
    }

    if ((SaveCounter > 69) && (SaveCounter < 80))
    {
      val = LEDTimersB[SaveCounter - 70];
    }

    if ((SaveCounter > 79) && (SaveCounter < 90))
    {
      val = LEDTimerValuesB[SaveCounter - 80];
    }

    if ((SaveCounter > 89) && (SaveCounter < 100))
    {
      val = FixedFadeA[SaveCounter - 90];
    }

    if ((SaveCounter > 99) && (SaveCounter < 110))
    {
      val = FixedFadeB[SaveCounter - 100];
    }

    entrytimer(EEPROMWRITE);
    EEPROM.write(SaveAddr, val); // this takes 2812us to run ??? (3400 with variable access???)
    exittimer(EEPROMWRITE);

    SaveAddr++;
    SaveCounter++;
    if (SaveCounter > 109)
    {
      SaveInProgress = 0;
      SaveCounter = 0;
      SaveAddr = 1930;
      Serial.println(F("SVE"));
    }
  }

  exittimer(TASKSAVEEEPROM);
}

void ForceWriteToEEPROM()
{
  SaveToEEPROM(1);
  for (byte i = 0; i < 255; i++)
  {
    SaveToEEPROM();
  }
  Serial.println(F("EEPROM INIT"));
}

void InitEEPROM()
{
  if ((EEPROM.read(1929) != 42) && (EEPROM.read(2041) != 42))
  {
    ForceWriteToEEPROM();
    EEPROM.write(1929, 42);
    EEPROM.write(2041, 42);
  }
}

//crazy but fast
void DecrementAddress()
{
  if (OneFrame[5] == '0')
  {
    OneFrame[5] = '9';
    if (OneFrame[4] == '0')
    {
      OneFrame[4] = '9';
      if (OneFrame[3] == '0')
      {
        OneFrame[3] = '9';
        if (OneFrame[2] == '0')
        {
          OneFrame[2] = '9';
          if (OneFrame[1] == '0')
          {
            OneFrame[1] = '9';
          }
          else
          {
            OneFrame[1]--;
          }
        }
        else
        {
          OneFrame[2]--;
        }
      }
      else
      {
        OneFrame[3]--;
      }
    }
    else
    {
      OneFrame[4]--;
    }
  }
  else
  {
    OneFrame[5]--;
  }
}

//// +PRG0800FFC          // WRITE 0xFF to addr 0x800
//// +PRG0FFF00D          // SAVE ARRAY VALUES TO EEPROM, RELOAD THEM ON STARTUP
void ProcessPRGMsg()
{
  int ADDR;
  byte VAL;

  ADDR = HexToDec(5, 7);
  VAL = HexToDec(8, 9);

  if ((ADDR == 4095) && (VAL == 0))
  {
    SaveToEEPROM(1);
  }
  else
  {
    EEPROM.write(ADDR, VAL);
  }
}

//BUG: remove this when done
byte printframe = 0;

//152 uS max execution time
byte PrintOneFrame(byte FrameReady = 0)
{
  static byte nextTime = 0;
  byte v = 0;

  entrytimer(TASKSERIALPRINT);

  if (FrameReady != 0)
  {
    printframe = 1;
    nextTime = 1;
  }
  else
  {
    if (nextTime)
    {
      Serial.println(OneFrame);
      printframe = 0;
      nextTime = 0;
      v = 1;
    }
  }

  exittimer(TASKSERIALPRINT);

  return v;
}

//BUG: remove this when done
byte printtimers = 0;

//64uS max execution time
void CmdStateMachine(char Rx)
{
  static byte CmdStateMachineState = 0;
  static byte CmdStateMachineBytesRead = 1;
  static byte CmdZeroAddress = 0;
  static byte CmdFFAddress = 0;
  static byte CmdCRC = 0;

  entrytimer(TASKCMDSM);

  int serialin = Serial.read();

  if (serialin != -1)
  {
    Rx = serialin;
  }
  else
  {
    return;
  }

  if ((Rx == '+') || (Rx == 13) || (Rx == 10))
  {
    CmdStateMachineState = CMDSMIDLE;
  }

  switch (CmdStateMachineState)
  {
    case CMDSMIDLE:
      {
        if (Rx == '+')
        {
          CmdStateMachineBytesRead = 1; // the + is implicitly included
          CmdZeroAddress = 0;
          CmdFFAddress = 0;
          CmdCRC = 0;

          CmdStateMachineState = CMDSMREADADDR;
        }
        break;
      }
    case CMDSMREADADDR:
      {
        OneFrame[CmdStateMachineBytesRead++] = Rx;

        CmdCRC += Rx;
        CmdCRC = CmdCRC ^ (CmdCRC >> 4);
        CmdCRC &= 15;

        if (Rx == '0')
        {
          CmdZeroAddress++;
        }
        else
        {
          CmdZeroAddress = 0;
        }

        if (Rx == 'F')
        {
          CmdFFAddress++;
        }
        else
        {
          CmdFFAddress = 0;
        }

        if (CmdStateMachineBytesRead == 6)
        {
          CmdStateMachineState = CMDSMREADREST;
        }
        break;
      }
    case CMDSMREADREST:
      {
        OneFrame[CmdStateMachineBytesRead++] = Rx;

        if (CmdStateMachineBytesRead < 11)
        {
          CmdCRC += Rx;
          CmdCRC = CmdCRC ^ (CmdCRC >> 4);
          CmdCRC &= 15;
        }

        if (CmdStateMachineBytesRead == 11)
        {
          bool isMulticast = false;

          if (CmdCRC == HexToDec(10, 10))
          {
            if (OneFrame[1] == 'P')
            {
              isMulticast = true;
              ProcessPRGMsg();
            }

            //BUG: pin 1 (TX) does not seem to be able be switched to input while Serial.begin is active
            if (OneFrame[1] == 'T')
            {
              isMulticast = true;

              //BUG: using T command to print timers, remove when done.
              printtimers = 1;
            }

            if ((CmdFFAddress == 5))
            {
              isMulticast = true;
            }

            if ((CmdZeroAddress == 5) || (CmdFFAddress == 5))
            {
              //packet for us, process
              ProcessFrame();
            }

            if ((CmdZeroAddress < 5) && (!isMulticast))
            {
              DecrementAddress();
              SetChecksum();
              PrintOneFrame(1);
            }

            if (isMulticast)
            {
              PrintOneFrame(1);
            }
          }

          CmdStateMachineState = CMDSMIDLE;
        }

        break;
      }
  }

  exittimer(TASKCMDSM);
}

void DoSerial()
{
  if (PrintOneFrame() == 0)
  {
    CmdStateMachine(0);
  }
}

//we have to each delaytime in us
//if printframe == 0, doSerial will take 64us
//if printframe == 1, doSerial will take 160us
void DelaySM(byte delaytime)
{
  unsigned long entry = micros();
  unsigned long t;

  if (printframe == 1)
  {
    if (delaytime < 155)
    {
      delayMicroseconds(delaytime);
      return;
    }
    else
    {
      DoSerial();
      t = micros() - entry;
      if (t < delaytime)
      {
        delayMicroseconds(delaytime - t);
      }
      return;
    }
  }
  else
  {
    if (delaytime < 64)
    {
      delayMicroseconds(delaytime);
      return;
    }
    else
    {
      do
      {
        DoSerial();
        t = micros() - entry;
      } while (((t + 64) <= delaytime) && (printframe == 0));
      t = micros() - entry;
      if (t < delaytime)
      {
        delayMicroseconds(delaytime - t);
      }
      return;
    }
  }
}

void UpdateOneLED()
{
  static byte LastSelectedLED = 0;

  if (LEDValuesA[0])
  {
    heartBeatOn();
  }
  else
  {
    heartBeatOff();
  }

  if (LastSelectedLED++ < 10)
  {
    byte rawvalue = 0;
    byte digit = LEDValuesA[LastSelectedLED];
    byte command = LEDCommandsA[LastSelectedLED];

    if (command == 0)
    {
      rawvalue = digit;
    }
    else
    {
      if ((digit == 0) && ((command == 4) || (command == 5)))
      {
        rawvalue = 0;
      }
      else
      {
        rawvalue = GetRawDigit(digit);
      }
    }

    byte onTimer;
    byte offTimer;

    if (FixedFadeA[LastSelectedLED] == 0)
    {
      onTimer = FadeTimer[LastSelectedLED];
      offTimer = 255 - onTimer;
    }
    else
    {
      onTimer = FixedFadeA[LastSelectedLED];
      offTimer = 255 - onTimer;
    }

    selectDigit(LastSelectedLED);
    displayRaw(rawvalue);
    DelaySM(onTimer);

    displayRaw(0); // eliminates ghosting on the next digit.
    selectDigit(255); // turn last digit off
    DelaySM(offTimer);
  }
  else
  {
    LastSelectedLED = 0;
  }
}

// command 0,1: just light up
// command 2: increment
// command 3: decrement
// command 4: increment, do not show zero
// command 5: decrement, do not show zero
// command 6: scroll down
// command 7: scroll up

void AnimateLED(byte SelectedLED)
{
  switch (LEDCommandsA[SelectedLED])
  {
    case 2:
    case 4:
      {
        if (--LEDTimersA[SelectedLED] == 255)
        {
          FadeTimer[SelectedLED] = 1;
          if (++LEDValuesA[SelectedLED] > 9)
          {
            LEDValuesA[SelectedLED] = 0;
          }
          LEDTimersA[SelectedLED] = LEDTimerValuesA[SelectedLED];
        }
        break;
      }
    case 3:
    case 5:
      {
        if (--LEDTimersA[SelectedLED] == 255)
        {
          FadeTimer[SelectedLED] = 1;
          if (--LEDValuesA[SelectedLED] == 255)
          {
            LEDValuesA[SelectedLED] = 9;
          }
          LEDTimersA[SelectedLED] = LEDTimerValuesA[SelectedLED];
        }
        break;
      }
  }
}

void AnimateOneLED()
{
  static unsigned int AnimateLEDTimer = AnimateLEDTimerValue;

  AnimateLEDTimer--;

  if (AnimateLEDTimer < 10)
  {
    AnimateLED(AnimateLEDTimer);
  }

  if (AnimateLEDTimer == 0)
  {
    AnimateLEDTimer = AnimateLEDTimerValue;
    SaveToEEPROM();       // BUG: Write one byte to eeprom once all the digits have been displayed, takes 3ms to execute, 37 seconds for whole data, no visual impactt
  }
}

void UpdateFade()
{
  static byte FadeTimerCnt = FadeTimerValue;

  FadeTimerCnt--;

  // update all LEDS as the value of FadeTimerCnt approaches 0
  if (FadeTimerCnt < 10)
  {
    if (FadeTimer[FadeTimerCnt] < 254)
    {
      FadeTimer[FadeTimerCnt]++;
    }
  }

  if (FadeTimerCnt == 0)
  {
    FadeTimerCnt = FadeTimerValue;
  }
}

//affects:
//byte LEDValues[10]
//byte LEDCommands[10]
//byte LEDTimers[10]

void ProcessFrame()
{
  static byte ArrayASelected = 1;

  byte LED, CMD, VAL;
  byte i, v;

  LED = HexToDec(6, 6);
  CMD = HexToDec(7, 7);
  VAL = HexToDec(8, 9);

  switch (CMD)
  {
    // Select A array
    case 0xA:
      {
        ArrayASelected = 1;
        break;
      }
    // Select B array
    case 0xB:
      {
        ArrayASelected = 0;
        break;
      }
    // copy arrays, do not trigger fade
    case 0xC:
      {
        if (VAL == 0)
        {
          for (i = 0; i < 10; i++)
          {
            LEDValuesA[i] = LEDValuesB[i];
            LEDCommandsA[i] = LEDCommandsB[i];
            LEDTimersA[i] = LEDTimersB[i];
            LEDTimerValuesA[i] = LEDTimerValuesB[i];
            FixedFadeA[i] = FixedFadeB[i];
          }
        }
        else
        {
          for (i = 0; i < 10; i++)
          {
            LEDValuesB[i] = LEDValuesA[i];
            LEDCommandsB[i] = LEDCommandsA[i];
            LEDTimersB[i] = LEDTimersA[i];
            LEDTimerValuesB[i] = LEDTimerValuesA[i];
            FixedFadeB[i] = FixedFadeA[i];
          }
        }
        break;
      }
    // Set delay
    case 0xD:
      {
        if (LED < 10) // LED is 0..9, single LED selected
        {
          if (ArrayASelected)
          {
            LEDTimersA[LED] = VAL;
            LEDTimerValuesA[LED] = VAL;
          }
          else
          {
            LEDTimersB[LED] = VAL;
            LEDTimerValuesB[LED] = VAL;
          }
        }
        else // LED is A, all LED selected
        {
          if (ArrayASelected)
          {
            for (byte i = 0; i < 10; i++)
            {
              LEDTimersA[i] = VAL;
              LEDTimerValuesA[i] = VAL;
            }
          }
          else
          {
            for (byte i = 0; i < 10; i++)
            {
              LEDTimersB[i] = VAL;
              LEDTimerValuesB[i] = VAL;
            }
          }
        }
        break;
      }
    // set fixed brightness level
    case 0xE:
      {
        if (LED < 10) // LED is 0..9, single LED selected
        {
          if (ArrayASelected)
          {
            FixedFadeA[LED] = VAL;
          }
          else
          {
            FixedFadeB[LED] = VAL;
          }
        }
        else // LED is A, all LED selected
        {
          if (ArrayASelected)
          {
            for (byte i = 0; i < 10; i++)
            {
              FixedFadeA[i] = VAL;
            }
          }
          else
          {
            for (byte i = 0; i < 10; i++)
            {
              FixedFadeB[i] = VAL;
            }
          }
        }
        break;
      }
    // Set flip A and B arrays
    case 0xF:
      {
        for (i = 0; i < 10; i++)
        {
          FadeTimer[i] = 1;

          v = LEDValuesB[i];
          LEDValuesB[i] = LEDValuesA[i];
          LEDValuesA[i] = v;

          v = LEDCommandsB[i];
          LEDCommandsB[i] = LEDCommandsA[i];
          LEDCommandsA[i] = v;

          v = LEDTimersB[i];
          LEDTimersB[i] = LEDTimersA[i];
          LEDTimersA[i] = v;

          v = LEDTimerValuesB[i];
          LEDTimerValuesB[i] = LEDTimerValuesA[i];
          LEDTimerValuesA[i] = v;

          v = FixedFadeB[i];
          FixedFadeB[i] = FixedFadeA[i];
          FixedFadeA[i] = v;
        }
        break;
      }
    //any other commands, write it to command and values and let AnimateLED deal with it...
    default:
      {
        if (LED < 10) // LED is 0..9, single LED selected
        {
          if (ArrayASelected)
          {
            FadeTimer[LED] = 1;         // only reset fade if writing to A array
            LEDCommandsA[LED] = CMD;
            LEDValuesA[LED] = VAL;
          }
          else
          {
            LEDCommandsB[LED] = CMD;
            LEDValuesB[LED] = VAL;
          }
        }
        else // LED is A, all LED selected
        {
          if (ArrayASelected)
          {
            for (byte i = 0; i < 10; i++)
            {
              FadeTimer[i] = 1;      // only reset fade if writing to A array
              LEDCommandsA[i] = CMD;
              LEDValuesA[i] = VAL;
            }
          }
          else
          {
            for (byte i = 0; i < 10; i++)
            {
              LEDCommandsB[i] = CMD;
              LEDValuesB[i] = VAL;
            }
          }
        }
        break;
      }
  }
}

void setup() {
  Serial.begin(250000);
  Serial.println(F("03242019"));

  allSegmentOff();
  allSegmentOutput();

  allDigitOff();
  allDigitOutput();

  heartBeatOutput();

  InitEEPROM();

  LoadFromEEPROM();

  dumpAllArrays();
}

void loop() {

  UpdateFade();
  UpdateOneLED();
  AnimateOneLED(); // call it every loop, exits with no action if timer has not reached correct value

  if (printtimers)
  {
    printtimers = 0;
    printtimer();
    dumpAllArrays();
  }
}